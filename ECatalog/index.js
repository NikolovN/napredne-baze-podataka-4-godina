const express = require("express");
const path = require("path");
const exphbs = require("express-handlebars");
const { rawListeners } = require("process");
const logger = require("./middleware/logger.js");
const client = require("./routes/api/cassandra-client.js");
const { kStringMaxLength } = require("buffer");
const cassandra = require("cassandra-driver");
const Uuid = require("cassandra-driver").types.Uuid;

function getUsers(max) {
  var users = [];

  for (var i = 0; i < max; i++) {
    var ime = "imeKorisnika" + i;
    var date = new Date(2021, 1, 18);
    var password = "password" + i;
    var email = "email" + i + "@elfak.rs";
    var prezime = "prezime" + i;
    users.push({
      ime: ime,
      prezime: prezime,
      email: email,
      password: password,
      datum_kreiranja: date,
    });
  }
  return users;
}

function getPhones(max) {
  var phones = [];

  for (var i = 0; i < max; i++) {
    var proizvodjac = "proizvodjac";
    if (i >= 0 && i < max / 5) {
      proizvodjac = proizvodjac + "0";
    } else if (i >= max / 5 && i < (2 * max) / 5) {
      proizvodjac = proizvodjac + "1";
    } else if (i >= (2 * max) / 5 && i < (3 * max) / 5) {
      proizvodjac = proizvodjac + "2";
    } else if (i >= (3 * max) / 5 && i < (4 * max) / 5) {
      proizvodjac = proizvodjac + "3";
    }
    var tip = "tip" + i;
    var tip_proizvoda = "Phones";
    var podtip;
    if (i >= 0 && i < max / 2) {
      podtip = "Smartphone";
    } else {
      podtip = "Flip phone";
    }
    var diagonala_ekrana = "diagonala_ekrana" + i;
    var ram_memorija = i + 1;
    var interna_memorija = 100 + i;
    var kamera = "kamera" + i + " Mpix";
    var kapacitet_baterije = (i + 1) * 100 + " mAh";
    var rezolucija = "1792 x 828";
    var tip_ekrana = "tip_ekrana" + i;
    var cena = "cena" + i;
    var tagovi;
    if (i >= 0 && i < max / 5) {
      tagovi = "apple black red purple";
    } else if (i >= max / 5 && i < (2 * max) / 5) {
      tagovi = "samsung black";
    } else if (i >= (2 * max) / 5 && i < (3 * max) / 5) {
      tagovi = "nokia purple";
    } else if (i >= (3 * max) / 5 && i < (4 * max) / 5) {
      tagovi = "alcatel red orrange blue";
    }

    var datum_dodavanja = new Date(2021, 1, 18);
    phones.push({
      proizvodjac,
      tip,
      tip_proizvoda,
      podtip,
      diagonala_ekrana,
      ram_memorija,
      interna_memorija,
      kamera,
      kapacitet_baterije,
      rezolucija,
      tip_ekrana,
      cena,
      tagovi,
      datum_dodavanja,
    });
  }

  return phones;
}

function getComputers(max) {
  var computers = [];

  for (var i = 0; i < max; i++) {
    var proizvodjac = "proizvodjac";
    if (i >= 0 && i < max / 5) {
      proizvodjac = proizvodjac + "0";
    } else if (i >= max / 5 && i < (2 * max) / 5) {
      proizvodjac = proizvodjac + "1";
    } else if (i >= (2 * max) / 5 && i < (3 * max) / 5) {
      proizvodjac = proizvodjac + "2";
    } else if (i >= (3 * max) / 5 && i < (4 * max) / 5) {
      proizvodjac = proizvodjac + "3";
    }
    var tip = "tip" + i;
    var diagonala_ekrana;
    var tip_proizvoda = "computers";
    var podtip;
    if (i >= 0 && i < max / 2) {
      podtip = "Laptop";
      diagonala_ekrana = "diagonala_ekrana" + i;
    } else {
      podtip = "PC";
      diagonala_ekrana = "";
    }
    var model_procesora = "model_procesora" + i;
    var tip_graficke_kartice = "tip_graficke_kartice" + i;
    var ram_memorija = i + 1;
    var hdd_ssd = "hdd_ssd" + i;
    var cena = "cena" + i;
    var tagovi;
    if (i >= 0 && i < max / 5) {
      tagovi = "hp black red purple";
    } else if (i >= max / 5 && i < (2 * max) / 5) {
      tagovi = "lenovo black";
    } else if (i >= (2 * max) / 5 && i < (3 * max) / 5) {
      tagovi = "asus purple";
    } else if (i >= (3 * max) / 5 && i < (4 * max) / 5) {
      tagovi = "hp black blue red";
    }

    var datum_dodavanja = new Date(2021, 1, 18);
    computers.push({
      proizvodjac,
      tip,
      tip_proizvoda,
      podtip,
      model_procesora,
      diagonala_ekrana,
      tip_graficke_kartice,
      ram_memorija,
      hdd_ssd,
      cena,
      tagovi,
      datum_dodavanja,
    });
  }

  return computers;
}

function getTvs(max) {
  var tvs = [];

  for (var i = 0; i < max; i++) {
    var proizvodjac = "proizvodjac";
    if (i >= 0 && i < max / 5) {
      proizvodjac = proizvodjac + "0";
    } else if (i >= max / 5 && i < (2 * max) / 5) {
      proizvodjac = proizvodjac + "1";
    } else if (i >= (2 * max) / 5 && i < (3 * max) / 5) {
      proizvodjac = proizvodjac + "2";
    } else if (i >= (3 * max) / 5 && i < (4 * max) / 5) {
      proizvodjac = proizvodjac + "3";
    }
    var tip = "tip" + i;
    var diagonala_ekrana;
    var tip_proizvoda = "tvs";
    var podtip;
    if (i >= 0 && i < max / 2) {
      podtip = "Plasma";
      diagonala_ekrana = "diagonala_ekrana" + i;
    } else {
      podtip = "Lcd";
      diagonala_ekrana = "";
    }
    var tip_ekrana = "tip_ekrana" + i;
    var rezolucija = "rezolucija" + i;
    var digitalni_tjuner = "digitalni_tjuner" + 1;
    var operativni_sistem = "operativni_sistem" + i;
    var cena = "cena" + i;
    var tagovi;
    if (i >= 0 && i < max / 5) {
      tagovi = "samsung black red purple";
    } else if (i >= max / 5 && i < (2 * max) / 5) {
      tagovi = "samsung black";
    } else if (i >= (2 * max) / 5 && i < (3 * max) / 5) {
      tagovi = "vox purple";
    } else if (i >= (3 * max) / 5 && i < (4 * max) / 5) {
      tagovi = "samsung internet plasma";
    }

    var datum_dodavanja = new Date(2021, 1, 18);
    tvs.push({
      proizvodjac,
      tip,
      tip_proizvoda,
      podtip,
      tip_ekrana,
      diagonala_ekrana,
      rezolucija,
      digitalni_tjuner,
      operativni_sistem,
      cena,
      tagovi,
      datum_dodavanja,
    });
  }

  return tvs;
}

const app = express();

app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("view engine", "handlebars");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  if (req.body.msg != undefined || req.body.msg != null) {
    client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then((result) => {
      const msg = "Ulogovan je korisnik sa id-em: " + result.rows[0].korisnikid.toString();
      res.render("index", { msg });
    });
  } else {
    res.redirect("/api/login");
  }
});

app.get("/api/create-update-phones/update/:tipprodukta/:podtiprodukta/:proizvodjac/:tip", (req, res) => {
  //client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then((result) => {
  //const korisnikid = "Ulogovan je korisnik sa id-em: " + result.rows[0].korisnikid.toString();
  //console.log(korisnikid);
  const tipprodukta = req.params.tipprodukta;
  const podtipprodukta = req.params.podtiprodukta;
  const proizvodjac = req.params.proizvodjac;
  const tip = req.params.tip;
  const params = {
    tipprodukta,
    podtipprodukta,
    proizvodjac,
    tip,
  };
  console.log(params);
  client
    .execute("SELECT * FROM ecatalog.phones WHERE proizvodjac = ? AND tip = ?", [proizvodjac, tip], {
      prepare: true,
    })
    .then(function (result) {
      var phone = {};
      const array = [];
      result.rows[0].tagovi.forEach((tag) => {
        array.push(tag);
      });

      var resultat = "";
      array.forEach((elem) => {
        resultat = resultat + " " + elem;
      });

      var olddatumdodavanja = JSON.stringify(result.rows[0].datum_dodavanja);
      olddatumdodavanja = JSON.parse(olddatumdodavanja);
      var korisnikid = result.rows[0].korisnikid;
      phone = {
        proizvodjac: result.rows[0].proizvodjac,
        tip: result.rows[0].tip,
        tip_proizvoda: result.rows[0].tip_proizvoda,
        podtip_proizvoda: result.rows[0].podtip,
        diagonala_ekrana: result.rows[0].diagonala_ekrana,
        ram_memorija: result.rows[0].ram_memorija,
        interna_memorija: result.rows[0].interna_memorija,
        kamera: result.rows[0].kamera,
        kapacitet_baterije: result.rows[0].kapacitet_baterije,
        rezolucija: result.rows[0].rezolucija,
        tip_ekrana: result.rows[0].tip_ekrana,
        cena: result.rows[0].cena,
        tagovi: resultat,
        datum_dodavanja: olddatumdodavanja,
      };
      console.log({
        olddatumdodavanja,
        rezultat: phone.tagovi,
        korisnikid,
        method: "update",
        title: "Update phone",
        phone,
        params,
      });
      res.render("create-update-phones", {
        olddatumdodavanja,
        rezultat: phone.tagovi,
        korisnikid,
        method: "update",
        title: "Update phone",
        phone,
        params,
        readonly: "readonly",
      });
    });
  //});
});

function getTagsArray(tags) {
  var words = [];
  words = tags.split(" ");
  return words;
}

app.get("/api/create-update-phones/delete/:tipprodukta/:podtiprodukta/:proizvodjac/:tip", (req, res) => {
  const tipprodukta = req.params.tipprodukta;
  const podtipprodukta = req.params.podtiprodukta;
  const proizvodjac = req.params.proizvodjac;
  const tip = req.params.tip;
  client
    .execute(
      "SELECT korisnikid, datum_dodavanja, tagovi FROM ecatalog.phones WHERE proizvodjac = ? AND tip = ?",
      [proizvodjac, tip],
      {
        prepare: true,
      }
    )
    .then((result) => {
      const korisnikid = result.rows[0].korisnikid.toString();
      const datum_dodavanja = result.rows[0].datum_dodavanja;
      var tagovi = [];
      result.rows[0].tagovi.forEach((tag) => {
        tagovi.push(tag);
      });
      console.log(tagovi);
      client.execute("DELETE FROM ecatalog.phones WHERE proizvodjac = ? AND tip = ?", [proizvodjac, tip]).then(() => {
        console.log("Deleted row from ecatalog.phones");
        client
          .execute(
            "DELETE FROM ecatalog.proizvodi_po_datumu_dodavanja WHERE tip_proizvoda = ? AND podtip = ? AND datum_dodavanja = ? AND tip = ?",
            [tipprodukta, podtipprodukta, datum_dodavanja, tip]
          )
          .then(() => {
            console.log("Deleted row from ecatalog.proizvodi_po_datumu_dodavanja");
            client
              .execute(
                "DELETE FROM ecatalog.korisnik_dodao_produkt WHERE korisnikid = ? AND tip = ? AND added_date = ?",
                [korisnikid, tip, datum_dodavanja]
              )
              .then(() => {
                console.log("Deleted row from ecatalog.korisnik_dodao_produkt");
                console.log("Tagovi", tagovi);
                tagovi.forEach((tag) => {
                  client.execute(
                    "DELETE FROM ecatalog.proizvodi_po_tagovima WHERE tag = ? AND tip = ? AND datum_dodavanja = ?",
                    [tag, tip, datum_dodavanja]
                  );
                });
                console.log("Deleted rows in ecatalog.proizvodi_po_tagovima");
                res.render("index", { alertType: "success", msg: "Phone has been deleted" });
              });
          });
      });
    });
});

app.get("/api/create-update-computers/update/:tipprodukta/:podtiprodukta/:proizvodjac/:tip", (req, res) => {
  const tipprodukta = req.params.tipprodukta;
  const podtipprodukta = req.params.podtiprodukta;
  const proizvodjac = req.params.proizvodjac;
  const tip = req.params.tip;
  const params = {
    tipprodukta,
    podtipprodukta,
    proizvodjac,
    tip,
  };
  console.log(params);
  client
    .execute("SELECT * FROM ecatalog.computers WHERE proizvodjac = ? AND tip = ?", [proizvodjac, tip], {
      prepare: true,
    })
    .then(function (result) {
      var computer = {};
      const array = [];
      result.rows[0].tagovi.forEach((tag) => {
        array.push(tag);
      });

      var resultat = "";
      array.forEach((elem) => {
        resultat = resultat + " " + elem;
      });

      var olddatumdodavanja = JSON.stringify(result.rows[0].datum_dodavanja);
      olddatumdodavanja = JSON.parse(olddatumdodavanja);
      var korisnikid = result.rows[0].korisnikid;
      console.log("hdd_ssd", result.rows[0].hdd_ssd);
      computer = {
        proizvodjac: result.rows[0].proizvodjac,
        tip: result.rows[0].tip,
        tip_proizvoda: result.rows[0].tip_proizvoda,
        podtip_proizvoda: result.rows[0].podtip,
        model_procesora: result.rows[0].model_procesora,
        diagonala_ekrana: result.rows[0].diagonala_ekrana,
        tip_graficke_kartice: result.rows[0].tip_graficke_kartice,
        ram_memorija: result.rows[0].ram_memorija,
        hdd_ssd: result.rows[0].hdd_ssd,
        cena: result.rows[0].cena,
        tagovi: resultat,
        datum_dodavanja: olddatumdodavanja,
      };
      console.log({
        olddatumdodavanja,
        rezultat: computer.tagovi,
        korisnikid,
        method: "update",
        title: "Update computer",
        computer,
        params,
      });
      res.render("create-update-computers", {
        olddatumdodavanja,
        rezultat: computer.tagovi,
        korisnikid,
        method: "update",
        title: "Update computer",
        computer,
        params,
        readonly: "readonly",
      });
    });
  //});
});

function getTagsArray(tags) {
  var words = [];
  words = tags.split(" ");
  return words;
}

app.get("/api/create-update-computers/delete/:tipprodukta/:podtiprodukta/:proizvodjac/:tip", (req, res) => {
  const tipprodukta = req.params.tipprodukta;
  const podtipprodukta = req.params.podtiprodukta;
  const proizvodjac = req.params.proizvodjac;
  const tip = req.params.tip;
  client
    .execute(
      "SELECT korisnikid, datum_dodavanja, tagovi FROM ecatalog.computers WHERE proizvodjac = ? AND tip = ?",
      [proizvodjac, tip],
      {
        prepare: true,
      }
    )
    .then((result) => {
      const korisnikid = result.rows[0].korisnikid.toString();
      const datum_dodavanja = result.rows[0].datum_dodavanja;
      var tagovi = [];
      result.rows[0].tagovi.forEach((tag) => {
        tagovi.push(tag);
      });
      console.log(tagovi);
      client
        .execute("DELETE FROM ecatalog.computers WHERE proizvodjac = ? AND tip = ?", [proizvodjac, tip])
        .then(() => {
          console.log("Deleted row from ecatalog.computers");
          client
            .execute(
              "DELETE FROM ecatalog.proizvodi_po_datumu_dodavanja WHERE tip_proizvoda = ? AND podtip = ? AND datum_dodavanja = ? AND tip = ?",
              [tipprodukta, podtipprodukta, datum_dodavanja, tip]
            )
            .then(() => {
              console.log("Deleted row from ecatalog.proizvodi_po_datumu_dodavanja");
              client
                .execute(
                  "DELETE FROM ecatalog.korisnik_dodao_produkt WHERE korisnikid = ? AND tip = ? AND added_date = ?",
                  [korisnikid, tip, datum_dodavanja]
                )
                .then(() => {
                  console.log("Deleted row from ecatalog.korisnik_dodao_produkt");
                  console.log("Tagovi", tagovi);
                  tagovi.forEach((tag) => {
                    client.execute(
                      "DELETE FROM ecatalog.proizvodi_po_tagovima WHERE tag = ? AND tip = ? AND datum_dodavanja = ?",
                      [tag, tip, datum_dodavanja]
                    );
                  });
                  console.log("Deleted rows in ecatalog.proizvodi_po_tagovima");
                  res.render("index", { alertType: "success", msg: "Computer has been deleted" });
                });
            });
        });
    });
});

app.get("/api/create-update-tvs/update/:tipprodukta/:podtiprodukta/:proizvodjac/:tip", (req, res) => {
  const tipprodukta = req.params.tipprodukta;
  const podtipprodukta = req.params.podtiprodukta;
  const proizvodjac = req.params.proizvodjac;
  const tip = req.params.tip;
  const params = {
    tipprodukta,
    podtipprodukta,
    proizvodjac,
    tip,
  };
  console.log(params);
  client
    .execute("SELECT * FROM ecatalog.tvs WHERE proizvodjac = ? AND tip = ?", [proizvodjac, tip], {
      prepare: true,
    })
    .then(function (result) {
      var tv = {};
      const array = [];
      result.rows[0].tagovi.forEach((tag) => {
        array.push(tag);
      });

      var resultat = "";
      array.forEach((elem) => {
        resultat = resultat + " " + elem;
      });

      var olddatumdodavanja = JSON.stringify(result.rows[0].datum_dodavanja);
      olddatumdodavanja = JSON.parse(olddatumdodavanja);
      var korisnikid = result.rows[0].korisnikid;
      tv = {
        proizvodjac: result.rows[0].proizvodjac,
        tip: result.rows[0].tip,
        tip_proizvoda: result.rows[0].tip_proizvoda,
        podtip_proizvoda: result.rows[0].podtip,
        tip_ekrana: result.rows[0].tip_ekrana,
        diagonala_ekrana: result.rows[0].diagonala_ekrana,
        rezolucija: result.rows[0].rezolucija,
        digitalni_tjuner: result.rows[0].digitalni_tjuner,
        operativni_sistem: result.rows[0].operativni_sistem,
        cena: result.rows[0].cena,
        tagovi: resultat,
        datum_dodavanja: olddatumdodavanja,
      };
      console.log({
        olddatumdodavanja,
        rezultat: tv.tagovi,
        korisnikid,
        method: "update",
        title: "Update TV",
        tv,
        params,
      });
      res.render("create-update-tvs", {
        olddatumdodavanja,
        rezultat: tv.tagovi,
        korisnikid,
        method: "update",
        title: "Update TV",
        tv,
        params,
        readonly: "readonly",
      });
    });
  //});
});

function getTagsArray(tags) {
  var words = [];
  words = tags.split(" ");
  return words;
}

app.get("/api/create-update-tvs/delete/:tipprodukta/:podtiprodukta/:proizvodjac/:tip", (req, res) => {
  const tipprodukta = req.params.tipprodukta;
  const podtipprodukta = req.params.podtiprodukta;
  const proizvodjac = req.params.proizvodjac;
  const tip = req.params.tip;
  client
    .execute(
      "SELECT korisnikid, datum_dodavanja, tagovi FROM ecatalog.tvs WHERE proizvodjac = ? AND tip = ?",
      [proizvodjac, tip],
      {
        prepare: true,
      }
    )
    .then((result) => {
      const korisnikid = result.rows[0].korisnikid.toString();
      const datum_dodavanja = result.rows[0].datum_dodavanja;
      var tagovi = [];
      result.rows[0].tagovi.forEach((tag) => {
        tagovi.push(tag);
      });
      console.log(tagovi);
      client.execute("DELETE FROM ecatalog.tvs WHERE proizvodjac = ? AND tip = ?", [proizvodjac, tip]).then(() => {
        console.log("Deleted row from ecatalog.tvs");
        client
          .execute(
            "DELETE FROM ecatalog.proizvodi_po_datumu_dodavanja WHERE tip_proizvoda = ? AND podtip = ? AND datum_dodavanja = ? AND tip = ?",
            [tipprodukta, podtipprodukta, datum_dodavanja, tip]
          )
          .then(() => {
            console.log("Deleted row from ecatalog.proizvodi_po_datumu_dodavanja");
            client
              .execute(
                "DELETE FROM ecatalog.korisnik_dodao_produkt WHERE korisnikid = ? AND tip = ? AND added_date = ?",
                [korisnikid, tip, datum_dodavanja]
              )
              .then(() => {
                console.log("Deleted row from ecatalog.korisnik_dodao_produkt");
                console.log("Tagovi", tagovi);
                tagovi.forEach((tag) => {
                  client.execute(
                    "DELETE FROM ecatalog.proizvodi_po_tagovima WHERE tag = ? AND tip = ? AND datum_dodavanja = ?",
                    [tag, tip, datum_dodavanja]
                  );
                });
                console.log("Deleted rows in ecatalog.proizvodi_po_tagovima");
                res.render("index", { alertType: "success", msg: "TV has been deleted" });
              });
          });
      });
    });
});

app.get("/api/profile", (req, res) => {
  client
    .execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0")
    .then(function (result) {
      const korisnikid = result.rows[0].korisnikid.toString();
      console.log(korisnikid);
      client
        .execute("SELECT * FROM ecatalog.korisnici WHERE korisnikid = ?", [korisnikid], { prepare: true })
        .then((result) => {
          console.log("Selected user from database with id: " + korisnikid);
          const user = {
            korisnikid: result.rows[0].korisnikid,
            firstName: result.rows[0].ime,
            lastName: result.rows[0].prezime,
            email: result.rows[0].email,
            password: result.rows[0].password,
          };

          client
            .execute("SELECT * FROM ecatalog.korisnik_dodao_produkt WHERE korisnikid = ?", [korisnikid], {
              prepare: true,
            })
            .then((result) => {
              products = [];
              console.log(result.rows);
              result.rows.forEach((row) => {
                const date = "Jan " + row.added_date.toString().substring(8, 15);
                products.push({
                  tip_proizvoda: row.tip_proizvoda,
                  podtip_proizvoda: row.podtip,
                  proizvodjac: row.proizvodjac,
                  tip: row.tip,
                  datum_dodavanja: date,
                });
              });
              console.log(products);
              res.render("profil", {
                user,
                products,
              });
            });
        });
    })
    .catch(function (err) {
      console.error("There was an error", err);
      return client.shutdown().then(() => {
        throw err;
      });
    });
});

app.get("/api/users/delete/:id", (req, res) => {
  const korisnikid = req.params.id;
  client
    .execute("SELECT email FROM ecatalog.korisnici WHERE korisnikid = ?", [korisnikid], { prepare: true })
    .then((result) => {
      const email = result.rows[0].email;
      client.execute("DELETE FROM ecatalog.korisnici WHERE korisnikid = ?", [korisnikid]).then(() => {
        client.execute("DELETE FROM ecatalog.korisnici_logovanje WHERE email = ?", [email]).then(() => {
          client
            .execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0", { prepare: true })
            .then((result) => {
              const id = result.rows[0].korisnikid.toString();
              if (id === korisnikid) {
                console.log("id", id);
                console.log("korisnickiid", korisnikid);
                client
                  .execute("UPDATE ecatalog.trenutno_ulogovani_korisnik SET korisnikid = ? WHERE id = 0", [null])
                  .then(() => {
                    res.render("login", {
                      alertType: "info",
                      msg: "Current user has been deleted, please login again",
                    });
                  });
              } else {
                res.render("index", { alertType: "success", msg: "User deleted" });
              }
            });
        });
      });
    });
});

app.get("/api/fillDatabase", (req, res) => {
  const korisnikid = req.params.id;
  var users = getUsers(50);
  console.log(users);
  client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then(() => {
    users.forEach((user) => {
      var date_time = user.datum_kreiranja;
      const id = Uuid.random();
      const newUser = {
        id: id,
        firstName: user.ime,
        lastName: user.prezime,
        email: user.email,
        password: user.password,
      };
      console.log(korisnikid);
      console.log(newUser);
      console.log("New user", newUser);
      client
        .execute(
          "CREATE KEYSPACE IF NOT EXISTS ecatalog WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '3' }"
        )
        .then(function () {
          const query =
            "CREATE TABLE IF NOT EXISTS ecatalog.korisnici (korisnikId uuid, ime text, prezime text, email text, datum_kreiranja timestamp, PRIMARY KEY (korisnikId))";
          return client.execute(query);
        })
        .then(function () {
          const query =
            "CREATE TABLE IF NOT EXISTS ecatalog.korisnici_logovanje (email text, password text, korisnikId uuid, PRIMARY KEY (email))";
          return client.execute(query);
        })
        .then(function () {
          const query =
            "INSERT INTO ECatalog.korisnici (korisnikId, ime, prezime, email, datum_kreiranja) VALUES (?, ?, ?, ?, ?)";
          return client.execute(query, [newUser.id, newUser.firstName, newUser.lastName, newUser.email, date_time]);
        })
        .then(function () {
          const query = "INSERT INTO ecatalog.korisnici_logovanje (email, password, korisnikId) VALUES (?, ?, ?)";
          return client.execute(query, [newUser.email, newUser.password, newUser.id], {
            prepare: true,
          });
        })
        .then(function () {
          return client.execute("SELECT * FROM ecatalog.korisnici");
        })
        .then(function (result) {
          result.rows.forEach((row) => {
            //console.log(row);
          });
        })
        .catch(function (err) {
          console.error("There was an error", err);
          return client.shutdown().then(() => {
            throw err;
          });
        });
    });

    var phones = getPhones(50);
    phones.forEach((phone) => {
      var tags = [];
      tags = getTagsArray(phone.tagovi);
      var date_time = phone.datum_dodavanja;
      console.log("First phone tag");
      const newPhone = {
        proizvodjac: phone.proizvodjac,
        tip: phone.tip,
        tip_proizvoda: "Phone",
        podtip_proizvoda: phone.podtip,
        diagonala_ekrana: phone.diagonala_ekrana,
        ram_memorija: phone.ram_memorija,
        interna_memorija: phone.interna_memorija,
        kamera: phone.kamera,
        kapacitet_baterije: phone.kapacitet_baterije,
        rezolucija: phone.rezolucija,
        tip_ekrana: phone.tip_ekrana,
        cena: phone.cena,
        tagovi: tags,
        datum_dodavanja: phone.datum_dodavanja,
      };
      console.log("New phone", newPhone);

      client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then((result) => {
        const korisnikid = result.rows[0].korisnikid.toString();
        console.log("Korisnik koji dodaje novi telefon: " + korisnikid);
        const query1 =
          "INSERT INTO ecatalog.phones (proizvodjac, tip, korisnikid, tip_proizvoda, podtip, diagonala_ekrana, ram_memorija, interna_memorija, kamera, kapacitet_baterije, rezolucija, tip_ekrana, cena, tagovi, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        const query2 =
          "INSERT INTO ecatalog.proizvodi_po_datumu_dodavanja (tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?)";
        const query3 =
          "INSERT INTO ecatalog.proizvodi_po_tagovima (tag, tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?)";
        const query4 =
          "INSERT INTO ecatalog.korisnik_dodao_produkt (korisnikid, tip_proizvoda, podtip, proizvodjac, tip, added_date) VALUES (?, ?, ?, ?, ?, ?)";
        const params1 = [
          newPhone.proizvodjac,
          newPhone.tip,
          korisnikid,
          newPhone.tip_proizvoda,
          newPhone.podtip_proizvoda,
          newPhone.diagonala_ekrana,
          parseInt(newPhone.ram_memorija),
          parseInt(newPhone.interna_memorija),
          newPhone.kamera,
          newPhone.kapacitet_baterije,
          newPhone.rezolucija,
          newPhone.tip_ekrana,
          newPhone.cena,
          newPhone.tagovi,
          date_time,
        ];

        const params2 = [
          newPhone.tip_proizvoda,
          newPhone.podtip_proizvoda,
          newPhone.proizvodjac,
          newPhone.tip,
          date_time,
        ];

        const params4 = [
          korisnikid,
          newPhone.tip_proizvoda,
          newPhone.podtip_proizvoda,
          newPhone.proizvodjac,
          newPhone.tip,
          date_time,
        ];

        const queries = [
          { query: query1, params: params1 },
          { query: query2, params: params2 },
          { query: query4, params: params4 },
        ];
        client.batch(queries, { prepare: true }).then(function () {
          newPhone.tagovi.forEach((tag) => {
            const params3 = [
              tag,
              newPhone.tip_proizvoda,
              newPhone.podtip_proizvoda,
              newPhone.proizvodjac,
              newPhone.tip,
              date_time,
            ];
            client.execute(query3, params3);
          });
        });
      });
    });

    var computers = getComputers(50);
    computers.forEach((computer) => {
      var currentTime = new Date();
      var month = currentTime.getMonth() + 1;
      var day = currentTime.getDate();
      var year = currentTime.getFullYear();
      var tags = [];
      tags = getTagsArray(computer.tagovi);
      var date_time = new Date(year, month, day);
      console.log("First computer tag");
      const newComputer = {
        proizvodjac: computer.proizvodjac,
        tip: computer.tip,
        tip_proizvoda: "Computer",
        podtip_proizvoda: computer.podtip,
        model_procesora: computer.model_procesora,
        diagonala_ekrana: computer.diagonala_ekrana,
        tip_graficke_kartice: computer.tip_graficke_kartice,
        ram_memorija: computer.ram_memorija,
        hdd_ssd: computer.hdd_ssd,
        cena: computer.cena,
        tagovi: tags,
        datum_dodavanja: date_time,
      };
      console.log("New computer", newComputer);

      client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then((result) => {
        const korisnikid = result.rows[0].korisnikid.toString();
        console.log("Korisnik koji dodaje novi kompijuter: " + korisnikid);
        const query1 =
          "INSERT INTO ecatalog.computers (proizvodjac, tip, korisnikid, tip_proizvoda, podtip, model_procesora, diagonala_ekrana, tip_graficke_kartice, ram_memorija, hdd_ssd, cena, tagovi, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        const query2 =
          "INSERT INTO ecatalog.proizvodi_po_datumu_dodavanja (tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?)";
        const query3 =
          "INSERT INTO ecatalog.proizvodi_po_tagovima (tag, tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?)";
        const query4 =
          "INSERT INTO ecatalog.korisnik_dodao_produkt (korisnikid, tip_proizvoda, podtip, proizvodjac, tip, added_date) VALUES (?, ?, ?, ?, ?, ?)";
        const params1 = [
          newComputer.proizvodjac,
          newComputer.tip,
          korisnikid,
          newComputer.tip_proizvoda,
          newComputer.podtip_proizvoda,
          newComputer.model_procesora,
          newComputer.diagonala_ekrana,
          newComputer.tip_graficke_kartice,
          parseInt(newComputer.ram_memorija),
          newComputer.hdd_ssd,
          newComputer.cena,
          newComputer.tagovi,
          date_time,
        ];

        const params2 = [
          newComputer.tip_proizvoda,
          newComputer.podtip_proizvoda,
          newComputer.proizvodjac,
          newComputer.tip,
          date_time,
        ];

        const params4 = [
          korisnikid,
          newComputer.tip_proizvoda,
          newComputer.podtip_proizvoda,
          newComputer.proizvodjac,
          newComputer.tip,
          date_time,
        ];

        const queries = [
          { query: query1, params: params1 },
          { query: query2, params: params2 },
          { query: query4, params: params4 },
        ];
        client.batch(queries, { prepare: true }).then(function () {
          newComputer.tagovi.forEach((tag) => {
            const params3 = [
              tag,
              newComputer.tip_proizvoda,
              newComputer.podtip_proizvoda,
              newComputer.proizvodjac,
              newComputer.tip,
              date_time,
            ];
            client.execute(query3, params3);
          });
        });
      });
    });

    var tvs = getTvs(50);
    tvs.forEach((tv) => {
      var currentTime = new Date();
      var month = currentTime.getMonth() + 1;
      var day = currentTime.getDate();
      var year = currentTime.getFullYear();
      var tags = [];
      tags = getTagsArray(tv.tagovi);
      var date_time = new Date(year, month, day);
      console.log("First phone tag");
      const newTv = {
        proizvodjac: tv.proizvodjac,
        tip: tv.tip,
        tip_proizvoda: "TV",
        podtip_proizvoda: tv.podtip,
        tip_ekrana: tv.tip_ekrana,
        diagonala_ekrana: tv.diagonala_ekrana,
        rezolucija: tv.rezolucija,
        digitalni_tjuner: tv.digitalni_tjuner,
        operativni_sistem: tv.operativni_sistem,
        cena: tv.cena,
        tagovi: tags,
        datum_dodavanja: date_time,
      };
      console.log("New TV", newTv);

      client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then((result) => {
        const korisnikid = result.rows[0].korisnikid.toString();
        console.log("Korisnik koji dodaje novi tv: " + korisnikid);
        const query1 =
          "INSERT INTO ecatalog.tvs (proizvodjac, tip, korisnikid, tip_proizvoda, podtip, tip_ekrana, diagonala_ekrana, rezolucija, digitalni_tjuner, operativni_sistem, cena, tagovi, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        const query2 =
          "INSERT INTO ecatalog.proizvodi_po_datumu_dodavanja (tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?)";
        const query3 =
          "INSERT INTO ecatalog.proizvodi_po_tagovima (tag, tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?)";
        const query4 =
          "INSERT INTO ecatalog.korisnik_dodao_produkt (korisnikid, tip_proizvoda, podtip, proizvodjac, tip, added_date) VALUES (?, ?, ?, ?, ?, ?)";
        const params1 = [
          newTv.proizvodjac,
          newTv.tip,
          korisnikid,
          newTv.tip_proizvoda,
          newTv.podtip_proizvoda,
          newTv.tip_ekrana,
          newTv.diagonala_ekrana,
          newTv.rezolucija,
          newTv.digitalni_tjuner,
          newTv.operativni_sistem,
          newTv.cena,
          newTv.tagovi,
          date_time,
        ];

        const params2 = [newTv.tip_proizvoda, newTv.podtip_proizvoda, newTv.proizvodjac, newTv.tip, date_time];

        const params4 = [
          korisnikid,
          newTv.tip_proizvoda,
          newTv.podtip_proizvoda,
          newTv.proizvodjac,
          newTv.tip,
          date_time,
        ];

        const queries = [
          { query: query1, params: params1 },
          { query: query2, params: params2 },
          { query: query4, params: params4 },
        ];
        client.batch(queries, { prepare: true }).then(function () {
          newTv.tagovi.forEach((tag) => {
            const params3 = [tag, newTv.tip_proizvoda, newTv.podtip_proizvoda, newTv.proizvodjac, newTv.tip, date_time];
            client.execute(query3, params3);
          });
        });
      });
    });

    res.render("index", {
      alertType: "success",
      msg: "Base filled with fake data. Added 50 fake computers, phones, users and tvs",
    });
  });
});

app.use(express.static(path.join(__dirname, "public")));

app.use("/api/login", require("./routes/api/login"));
app.use("/api/register", require("./routes/api/register"));
app.use("/api/users", require("./routes/api/users"));
app.use("/api/products", require("./routes/api/products"));
app.use("/api/phones", require("./routes/api/phones"));
app.use("/api/create-update-phones", require("./routes/api/create-update-phones"));
app.use("/api/create-update-computers", require("./routes/api/create-update-computers"));
app.use("/api/create-update-tvs", require("./routes/api/create-update-tvs"));
app.use("/api/computers", require("./routes/api/computers"));
app.use("/api/tvs", require("./routes/api/tvs"));
app.use("/api/tags", require("./routes/api/tags"));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
