const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const Uuid = require("cassandra-driver").types.Uuid;
const uuid = require("uuid");
const express = require("express");
const routerCreateUpdatePhone = express.Router();
const Mapper = cassandra.mapping.Mapper;
var async = require("async");

//https://www.youtube.com/watch?v=voDummz1gO0&ab_channel=CodAffection

routerCreateUpdatePhone.get("/", (req, res) => {
  res.render("create-update-phones", {
    korisnikid: "",
    method: "create",
    title: "Create phone",
    phone: {
      proizvodjac: "Apple",
      tip: "iPhone XR",
      tip_proizvoda: "Phone",
      podtip_proizvoda: "Smartphone",
      diagonala_ekrana: "6.1",
      ram_memorija: 3,
      interna_memorija: 128,
      kamera: "212.0 Mpix",
      kapacitet_baterije: "2940 mAh",
      rezolucija: "1792 x 828",
      tip_ekrana: "IPS",
      cena: "86.390 RSD",
      tagovi: "apple iphone black orange white",
    },
    params: {},
    readonly: "",
  });
});

/*routerCreateUpdatePhone.get("/:id", (req, res) => {
  const query = "SELECT * FROM ecatalog.korisnici WHERE korisnikId = ?";
  const id = req.params.id;
  console.log("params", req.params.id);
  client.execute(query, [id], { prepare: true }).then(function (result) {
    res.render("register", {
      title: "Update user",
      user: {
        _id: result.rows[0].korisnikid.toString(),
        ime: result.rows[0].ime,
        prezime: result.rows[0].prezime,
        email: result.rows[0].email,
      },
    });
  });
});*/

routerCreateUpdatePhone.get("/:id", (req, res) => {
  console.log(req.params.id);
  res.render("index", { alertType: "info", msg: req.params.id });
});

function getTagsArray(tags) {
  var words = [];
  words = tags.split(" ");
  return words;
}

routerCreateUpdatePhone.post("/", (req, res) => {
  if (req.body.method === "update") {
    var olddatumdodavanja = req.body.olddatumdodavanja;
    ("2021-02-20T23:00:00.000Z");
    const year = olddatumdodavanja.substring(0, 4);
    const month = olddatumdodavanja.substring(5, 7);
    const day = olddatumdodavanja.substring(8, 10);
    var yearr = parseInt(year);
    var monthh = parseInt(month);
    var dayy = parseInt(day);
    console.log(yearr + " " + monthh + " " + dayy);
    olddatumdodavanja = new Date(yearr, monthh, dayy);
    console.log("Datum", olddatumdodavanja.toString());
    var tags = [];
    var oldkorisnikid = req.body.oldkorisnikid;
    tags = getTagsArray(req.body.tagovi);
    const updatedPhone = {
      proizvodjac: req.body.proizvodjac,
      tip: req.body.tip,
      korisnikid: oldkorisnikid,
      tip_proizvoda: "Phone",
      podtip_proizvoda: req.body.podtip_proizvoda,
      diagonala_ekrana: req.body.diagonala_ekrana,
      ram_memorija: req.body.ram_memorija,
      interna_memorija: req.body.interna_memorija,
      kamera: req.body.kamera,
      kapacitet_baterije: req.body.kapacitet_baterije,
      rezolucija: req.body.rezolucija,
      tip_ekrana: req.body.tip_ekrana,
      cena: req.body.cena,
      tagovi: tags,
      datum_dodavanja: olddatumdodavanja,
    };

    var array1 = [];
    updatedPhone.tagovi.forEach((tag) => {
      if (tag === "") {
      } else {
        array1.push(tag);
      }
    });
    updatedPhone.tagovi = array1;
    console.log("Updated phone", updatedPhone);
    client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then((result) => {
      const korisnikid = result.rows[0].korisnikid.toString();
      console.log("Korisnik koji dodaje novi telefon: " + korisnikid);
      const query1 =
        "UPDATE ecatalog.phones SET korisnikid = ?,  tip_proizvoda = ?, podtip = ?, diagonala_ekrana = ?, ram_memorija = ?, interna_memorija = ?, kamera = ?, kapacitet_baterije = ?, rezolucija = ?, tip_ekrana = ?, cena = ?, tagovi = ?, datum_dodavanja = ? WHERE proizvodjac = ? AND tip = ?";
      const query2 =
        "UPDATE ecatalog.proizvodi_po_datumu_dodavanja SET proizvodjac = ? WHERE tip_proizvoda = ? AND podtip = ? AND datum_dodavanja = ? AND tip = ?";
      const query3 =
        "INSERT INTO ecatalog.proizvodi_po_tagovima (tag, tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?)";
      const query4 =
        "UPDATE ecatalog.korisnik_dodao_produkt SET tip_proizvoda = ?, podtip = ?, proizvodjac = ? WHERE korisnikid = ? AND tip = ? AND added_date = ?";
      const params1 = [
        updatedPhone.korisnikid,
        updatedPhone.tip_proizvoda,
        updatedPhone.podtip_proizvoda,
        updatedPhone.diagonala_ekrana,
        parseInt(updatedPhone.ram_memorija),
        parseInt(updatedPhone.interna_memorija),
        updatedPhone.kamera,
        updatedPhone.kapacitet_baterije,
        updatedPhone.rezolucija,
        updatedPhone.tip_ekrana,
        updatedPhone.cena,
        updatedPhone.tagovi,
        date_time,
        req.body.oldproizvodjac,
        req.body.oldtip,
      ];

      const params2 = [
        updatedPhone.proizvodjac,
        req.body.oldtipprodukta,
        req.body.oldpodtipprodukta,
        olddatumdodavanja,
        updatedPhone.tip,
      ];

      const params4 = [
        updatedPhone.tip_proizvoda,
        updatedPhone.podtip_proizvoda,
        updatedPhone.proizvodjac,
        oldkorisnikid,
        req.body.oldtip,
        olddatumdodavanja,
      ];

      const queries = [
        { query: query1, params: params1 },
        { query: query2, params: params2 },
        { query: query4, params: params4 },
      ];
      client.batch(queries, { prepare: true }).then(function () {
        console.log("batch finished updating...");

        oldtags = getTagsArray(req.body.oldtagovi);
        oldtags = oldtags.filter((tagg) => {
          return tagg != "";
        });
        console.log(oldtags);
        oldtags.forEach((tag) => {
          console.log(tag);
          client.execute(
            "DELETE FROM ecatalog.proizvodi_po_tagovima WHERE tag = ? AND tip = ? AND datum_dodavanja = ?",
            [tag, req.body.oldtip, olddatumdodavanja]
          );
        });
        updatedPhone.tagovi = updatedPhone.tagovi.filter((tagg) => {
          return tagg != "";
        });
        updatedPhone.tagovi.forEach((tag) => {
          const params3 = [
            tag,
            updatedPhone.tip_proizvoda,
            updatedPhone.podtip_proizvoda,
            updatedPhone.proizvodjac,
            updatedPhone.tip,
            olddatumdodavanja,
          ];
          client.execute(query3, params3);
        });
        res.render("index", { alertType: "success", msg: "Phone has been updated: " + updatedPhone.tip });
      });
    });
  } else if (req.body.method === "create") {
    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var tags = [];
    tags = getTagsArray(req.body.tagovi);
    var date_time = new Date(year, month, day);
    console.log("First phone tag");
    const newPhone = {
      proizvodjac: req.body.proizvodjac,
      tip: req.body.tip,
      tip_proizvoda: "Phone",
      podtip_proizvoda: req.body.podtip_proizvoda,
      diagonala_ekrana: req.body.diagonala_ekrana,
      ram_memorija: req.body.ram_memorija,
      interna_memorija: req.body.interna_memorija,
      kamera: req.body.kamera,
      kapacitet_baterije: req.body.kapacitet_baterije,
      rezolucija: req.body.rezolucija,
      tip_ekrana: req.body.tip_ekrana,
      cena: req.body.cena,
      tagovi: tags,
      datum_dodavanja: date_time,
    };
    console.log("New phone", newPhone);

    client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then((result) => {
      const korisnikid = result.rows[0].korisnikid.toString();
      console.log("Korisnik koji dodaje novi telefon: " + korisnikid);
      const query1 =
        "INSERT INTO ecatalog.phones (proizvodjac, tip, korisnikid, tip_proizvoda, podtip, diagonala_ekrana, ram_memorija, interna_memorija, kamera, kapacitet_baterije, rezolucija, tip_ekrana, cena, tagovi, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
      const query2 =
        "INSERT INTO ecatalog.proizvodi_po_datumu_dodavanja (tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?)";
      const query3 =
        "INSERT INTO ecatalog.proizvodi_po_tagovima (tag, tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?)";
      const query4 =
        "INSERT INTO ecatalog.korisnik_dodao_produkt (korisnikid, tip_proizvoda, podtip, proizvodjac, tip, added_date) VALUES (?, ?, ?, ?, ?, ?)";
      const params1 = [
        newPhone.proizvodjac,
        newPhone.tip,
        korisnikid,
        newPhone.tip_proizvoda,
        newPhone.podtip_proizvoda,
        newPhone.diagonala_ekrana,
        parseInt(newPhone.ram_memorija),
        parseInt(newPhone.interna_memorija),
        newPhone.kamera,
        newPhone.kapacitet_baterije,
        newPhone.rezolucija,
        newPhone.tip_ekrana,
        newPhone.cena,
        newPhone.tagovi,
        date_time,
      ];

      const params2 = [
        newPhone.tip_proizvoda,
        newPhone.podtip_proizvoda,
        newPhone.proizvodjac,
        newPhone.tip,
        date_time,
      ];

      const params4 = [
        korisnikid,
        newPhone.tip_proizvoda,
        newPhone.podtip_proizvoda,
        newPhone.proizvodjac,
        newPhone.tip,
        date_time,
      ];

      const queries = [
        { query: query1, params: params1 },
        { query: query2, params: params2 },
        { query: query4, params: params4 },
      ];
      client.batch(queries, { prepare: true }).then(function () {
        newPhone.tagovi.forEach((tag) => {
          const params3 = [
            tag,
            newPhone.tip_proizvoda,
            newPhone.podtip_proizvoda,
            newPhone.proizvodjac,
            newPhone.tip,
            date_time,
          ];
          client.execute(query3, params3);
        });
        res.render("index", { alertType: "success", msg: "Phone has been created" });
      });
    });
  } else {
    res.render("index", { alertType: "danger", msg: "Something went wrong" });
  }
});

routerCreateUpdatePhone.put("/put/:id", (req, res) => {});

routerCreateUpdatePhone.get("/delete/:id", (req, res) => {});

module.exports = routerCreateUpdatePhone;
