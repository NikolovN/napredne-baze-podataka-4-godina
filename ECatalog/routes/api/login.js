const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const uuid = require("uuid");
const express = require("express");
const routerLogin = express.Router();
routerLogin.get("/", (req, res) => {
  res.render("login");
});

routerLogin.post("/", (req, res) => {
  const email = req.body.email;
  console.log(email);
  const id = 0;
  client
    .execute("SELECT korisnikid, password FROM ecatalog.korisnici_logovanje WHERE email = ?", [email])
    .then((result) => {
      if (result.rows[0] != undefined) {
        console.log(result.rows[0].password);
        console.log(req.body.password);
        if (result.rows[0].password != req.body.password) {
          const msg = `User with email: ${email}, doesn't have password: ${req.body.password}`;
          res.render("login", { alertType: "danger", msg: msg });
        } else {
          const id = result.rows[0].korisnikid.toString();
          console.log(id);
          client
            .execute("UPDATE ecatalog.trenutno_ulogovani_korisnik SET korisnikid = ? WHERE id = 0", [id])
            .then(() => {
              console.log("Ulogovao se novi korisnik sa id-em: " + id);
              console.log(email);
              res.render("index", { alertType: "primary", msg: `Ulogovan korisnik sa id-em: ${id}` });
            });
        }
      } else {
        const msg = `User with email: ${email}, not found`;
        res.render("login", { alertType: "danger", msg: msg });
      }
    });
});
routerLogin.put("/:id", (req, res) => {});
routerLogin.delete("/:id", (req, res) => {});

module.exports = routerLogin;
