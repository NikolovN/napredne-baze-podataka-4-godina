const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const uuid = require("uuid");
const express = require("express");
const routerComputers = express.Router();
const mapper = require("./mapper");

routerComputers.get("/", (req, res) => {
  client
    .execute("SELECT * FROM ecatalog.computers")
    .then(function (result) {
      computers = [];
      result.rows.forEach((row) => {
        const date = "Jan " + row.datum_dodavanja.toString().substring(8, 15);
        const array = [];
        const url_params = row.proizvodjac + row.tip;
        row.tagovi.forEach((tag) => {
          array.push(tag);
        });
        console.log(row.hdd_ssd);
        computers.push({
          url_params: url_params,
          proizvodjac: row.proizvodjac,
          tip: row.tip,
          tip_proizvoda: row.tip_proizvoda,
          podtip_proizvoda: row.podtip,
          model_procesora: row.model_procesora,
          diagonala_ekrana: row.diagonala_ekrana,
          tip_graficke_kartice: row.tip_graficke_kartice,
          ram_memorija: row.ram_memorija,
          hdd_ssd: row.hdd_ssd,
          cena: row.cena,
          tagovi: array,
          datum_dodavanja: date,
        });
      });
      res.render("computers", {
        computers,
      });
    })
    .catch(function (err) {
      console.error("There was an error", err);
      return client.shutdown().then(() => {
        throw err;
      });
    });
});

routerComputers.get("/:id", (req, res) => {});

routerComputers.post("/", (req, res) => {});

routerComputers.put("/:id", (req, res) => {});

routerComputers.delete("/:id", (req, res) => {});

module.exports = routerComputers;
