const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const uuid = require("uuid");
const express = require("express");
const routerProducts = express.Router();
const mapper = require("./mapper");

routerProducts.get("/", (req, res) => {
  client
    .execute("SELECT * FROM ecatalog.proizvodi_po_datumu_dodavanja")
    .then(function (result) {
      products = [];
      result.rows.forEach((row) => {
        const date = "Jan " + row.datum_dodavanja.toString().substring(8, 15);
	console.log(row.datum_dodavanja.toString());
        products.push({
          tip_proizvoda: row.tip_proizvoda,
          podtip_proizvoda: row.podtip,
          proizvodjac: row.proizvodjac,
          tip: row.tip,
          datum_dodavanja: date,
        });
      });
      console.log(products);
      res.render("products", {
        products,
      });
    })
    .catch(function (err) {
      console.error("There was an error", err);
      return client.shutdown().then(() => {
        throw err;
      });
    });
});

routerProducts.get("/:id", (req, res) => {});

routerProducts.post("/", (req, res) => {});

routerProducts.put("/:id", (req, res) => {});

routerProducts.delete("/:id", (req, res) => {});

module.exports = routerProducts;
