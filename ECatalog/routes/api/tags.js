const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const uuid = require("uuid");
const express = require("express");
const routerTags = express.Router();
const mapper = require("./mapper");

routerTags.get("/", (req, res) => {});

routerTags.get("/:tag", (req, res) => {
  const tag = req.params.tag;
  client
    .execute("SELECT * FROM ecatalog.proizvodi_po_tagovima WHERE tag = ?", [tag], { prepare: true })
    .then(function (result) {
      products = [];
      result.rows.forEach((row) => {
        const date = "Jan " + row.datum_dodavanja.toString().substring(8, 15);
        console.log(row.datum_dodavanja.toString());
        products.push({
          tip_proizvoda: row.tip_proizvoda,
          podtip_proizvoda: row.podtip,
          proizvodjac: row.proizvodjac,
          tip: row.tip,
          datum_dodavanja: date,
        });
      });
      console.log(products);
      res.render("tags", {
        tag,
        products,
      });
    })
    .catch(function (err) {
      console.error("There was an error", err);
      return client.shutdown().then(() => {
        throw err;
      });
    });
});

routerTags.post("/", (req, res) => {});

routerTags.put("/:id", (req, res) => {});

routerTags.delete("/:id", (req, res) => {});

module.exports = routerTags;
