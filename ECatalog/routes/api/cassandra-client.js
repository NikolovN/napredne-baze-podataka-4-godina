"use strict";
const cassandra = require("cassandra-driver");
var assert = require("assert");

const client = new cassandra.Client({
  contactPoints: ["127.0.0.1"],
  localDataCenter: "datacenter1",
  encoding: {
    map: Map,
    set: Set,
  },
});

module.exports = client;
