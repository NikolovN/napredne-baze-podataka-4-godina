const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const uuid = require("uuid");
const express = require("express");
const routerUsers = express.Router();
const mapper = require("./mapper");
var users = [];

routerUsers.get("/", (req, res) => {
  client
    .execute("SELECT * FROM ecatalog.korisnici")
    .then(function (result) {
      users = [];
      result.rows.forEach((row) => {
        console.log(row);
        const date = "Jan " + row.datum_kreiranja.toString().substring(8, 15);
        console.log(row.korisnikid.toString());
        users.push({
          korisnikid: row.korisnikid.toString(),
          ime: row.ime,
          prezime: row.prezime,
          email: row.email,
          datum_kreiranja: date,
        });
      });
      res.render("users", { users });
    })
    .catch(function (err) {
      console.error("There was an error", err);
      return client.shutdown().then(() => {
        throw err;
      });
    });
});

routerUsers.get("/:id", (req, res) => {});

routerUsers.post("/", (req, res) => {});

routerUsers.put("/:id", (req, res) => {});

routerUsers.delete("/:id", (req, res) => {});

module.exports = routerUsers;
