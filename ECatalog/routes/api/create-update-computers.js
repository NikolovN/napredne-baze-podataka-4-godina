const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const Uuid = require("cassandra-driver").types.Uuid;
const uuid = require("uuid");
const express = require("express");
const routerCreateUpdateComputer = express.Router();
const Mapper = cassandra.mapping.Mapper;
var async = require("async");

routerCreateUpdateComputer.get("/", (req, res) => {
  res.render("create-update-computers", {
    korisnikid: "",
    method: "create",
    title: "Create computer",
    computer: {
      proizvodjac: "Apple",
      tip: "MacBook Air 13 Retina",
      tip_proizvoda: "Computer",
      podtip_proizvoda: "Laptop",
      model_procesora: "Intel® Core™ i3 1000NG4 do 3.2GHz",
      diagonala_ekrana: "13.3",
      tip_graficke_kartice: "Integrisana Iris Plus Graphics G4",
      ram_memorija: 8,
      hdd_ssd: "256GB SSD",
      cena: "86.390 RSD",
      tagovi: "apple iphone black orange white",
    },
    params: {},
    readonly: "",
  });
});

routerCreateUpdateComputer.get("/:id", (req, res) => {
  console.log(req.params.id);
  res.render("index", { alertType: "info", msg: req.params.id });
});

function getTagsArray(tags) {
  var words = [];
  words = tags.split(" ");
  return words;
}

routerCreateUpdateComputer.post("/", (req, res) => {
  if (req.body.method === "update") {
    var olddatumdodavanja = req.body.olddatumdodavanja;
    ("2021-02-20T23:00:00.000Z");
    const year = olddatumdodavanja.substring(0, 4);
    const month = olddatumdodavanja.substring(5, 7);
    const day = olddatumdodavanja.substring(8, 10);
    var yearr = parseInt(year);
    var monthh = parseInt(month);
    var dayy = parseInt(day);
    console.log(yearr + " " + monthh + " " + dayy);
    olddatumdodavanja = new Date(yearr, monthh, dayy);
    console.log("Datum", olddatumdodavanja.toString());
    var tags = [];
    var oldkorisnikid = req.body.oldkorisnikid;
    tags = getTagsArray(req.body.tagovi);
    const updatedComputer = {
      proizvodjac: req.body.proizvodjac,
      tip: req.body.tip,
      korisnikid: oldkorisnikid,
      tip_proizvoda: "Computer",
      podtip_proizvoda: req.body.podtip_proizvoda,
      model_procesora: req.body.model_procesora,
      diagonala_ekrana: req.body.diagonala_ekrana,
      tip_graficke_kartice: req.body.tip_graficke_kartice,
      ram_memorija: req.body.ram_memorija,
      hdd_ssd: req.body.hdd_ssd,
      cena: req.body.cena,
      tagovi: tags,
      datum_dodavanja: olddatumdodavanja,
    };

    var array1 = [];
    updatedComputer.tagovi.forEach((tag) => {
      if (tag === "") {
      } else {
        array1.push(tag);
      }
    });
    updatedComputer.tagovi = array1;
    console.log("Updated computer", updatedComputer);
    client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then((result) => {
      const korisnikid = result.rows[0].korisnikid.toString();
      console.log("Korisnik koji dodaje novi telefon: " + korisnikid);
      const query1 =
        "UPDATE ecatalog.computers SET korisnikid = ?,  tip_proizvoda = ?, podtip = ?,  model_procesora = ?, diagonala_ekrana = ?, tip_graficke_kartice = ?, ram_memorija = ?, hdd_ssd = ?, cena = ?, tagovi = ?, datum_dodavanja = ? WHERE proizvodjac = ? AND tip = ?";
      const query2 =
        "UPDATE ecatalog.proizvodi_po_datumu_dodavanja SET proizvodjac = ? WHERE tip_proizvoda = ? AND podtip = ? AND datum_dodavanja = ? AND tip = ?";
      const query3 =
        "INSERT INTO ecatalog.proizvodi_po_tagovima (tag, tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?)";
      const query4 =
        "UPDATE ecatalog.korisnik_dodao_produkt SET tip_proizvoda = ?, podtip = ?, proizvodjac = ? WHERE korisnikid = ? AND tip = ? AND added_date = ?";
      const params1 = [
        updatedComputer.korisnikid,
        updatedComputer.tip_proizvoda,
        updatedComputer.podtip_proizvoda,
        updatedComputer.model_procesora,
        updatedComputer.diagonala_ekrana,
        updatedComputer.tip_graficke_kartice,
        parseInt(updatedComputer.ram_memorija),
        updatedComputer.hdd_ssd,
        updatedComputer.cena,
        updatedComputer.tagovi,
        date_time,
        req.body.oldproizvodjac,
        req.body.oldtip,
      ];

      const params2 = [
        updatedComputer.proizvodjac,
        req.body.oldtipprodukta,
        req.body.oldpodtipprodukta,
        olddatumdodavanja,
        updatedComputer.tip,
      ];

      const params4 = [
        updatedComputer.tip_proizvoda,
        updatedComputer.podtip_proizvoda,
        updatedComputer.proizvodjac,
        oldkorisnikid,
        req.body.oldtip,
        olddatumdodavanja,
      ];

      const queries = [
        { query: query1, params: params1 },
        { query: query2, params: params2 },
        { query: query4, params: params4 },
      ];
      client.batch(queries, { prepare: true }).then(function () {
        console.log("batch finished updating...");

        oldtags = getTagsArray(req.body.oldtagovi);
        oldtags = oldtags.filter((tagg) => {
          return tagg != "";
        });
        console.log(oldtags);
        oldtags.forEach((tag) => {
          console.log(tag);
          client.execute(
            "DELETE FROM ecatalog.proizvodi_po_tagovima WHERE tag = ? AND tip = ? AND datum_dodavanja = ?",
            [tag, req.body.oldtip, olddatumdodavanja]
          );
        });
        updatedComputer.tagovi = updatedComputer.tagovi.filter((tagg) => {
          return tagg != "";
        });
        updatedComputer.tagovi.forEach((tag) => {
          const params3 = [
            tag,
            updatedComputer.tip_proizvoda,
            updatedComputer.podtip_proizvoda,
            updatedComputer.proizvodjac,
            updatedComputer.tip,
            olddatumdodavanja,
          ];
          client.execute(query3, params3);
        });
        res.render("index", { alertType: "success", msg: "Computer has been updated: " + updatedComputer.tip });
      });
    });
  } else if (req.body.method === "create") {
    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var tags = [];
    tags = getTagsArray(req.body.tagovi);
    var date_time = new Date(year, month, day);
    console.log("First phone tag");
    const newComputer = {
      proizvodjac: req.body.proizvodjac,
      tip: req.body.tip,
      tip_proizvoda: "Computer",
      podtip_proizvoda: req.body.podtip_proizvoda,
      model_procesora: req.body.model_procesora,
      diagonala_ekrana: req.body.diagonala_ekrana,
      tip_graficke_kartice: req.body.tip_graficke_kartice,
      ram_memorija: req.body.ram_memorija,
      hdd_ssd: req.body.hdd_ssd,
      cena: req.body.cena,
      tagovi: tags,
      datum_dodavanja: date_time,
    };
    console.log("New computer", newComputer);

    client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then((result) => {
      const korisnikid = result.rows[0].korisnikid.toString();
      console.log("Korisnik koji dodaje novi kompijuter: " + korisnikid);
      const query1 =
        "INSERT INTO ecatalog.computers (proizvodjac, tip, korisnikid, tip_proizvoda, podtip, model_procesora, diagonala_ekrana, tip_graficke_kartice, ram_memorija, hdd_ssd, cena, tagovi, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
      const query2 =
        "INSERT INTO ecatalog.proizvodi_po_datumu_dodavanja (tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?)";
      const query3 =
        "INSERT INTO ecatalog.proizvodi_po_tagovima (tag, tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?)";
      const query4 =
        "INSERT INTO ecatalog.korisnik_dodao_produkt (korisnikid, tip_proizvoda, podtip, proizvodjac, tip, added_date) VALUES (?, ?, ?, ?, ?, ?)";
      const params1 = [
        newComputer.proizvodjac,
        newComputer.tip,
        korisnikid,
        newComputer.tip_proizvoda,
        newComputer.podtip_proizvoda,
        newComputer.model_procesora,
        newComputer.diagonala_ekrana,
        newComputer.tip_graficke_kartice,
        parseInt(newComputer.ram_memorija),
        newComputer.hdd_ssd,
        newComputer.cena,
        newComputer.tagovi,
        date_time,
      ];

      const params2 = [
        newComputer.tip_proizvoda,
        newComputer.podtip_proizvoda,
        newComputer.proizvodjac,
        newComputer.tip,
        date_time,
      ];

      const params4 = [
        korisnikid,
        newComputer.tip_proizvoda,
        newComputer.podtip_proizvoda,
        newComputer.proizvodjac,
        newComputer.tip,
        date_time,
      ];

      const queries = [
        { query: query1, params: params1 },
        { query: query2, params: params2 },
        { query: query4, params: params4 },
      ];
      client.batch(queries, { prepare: true }).then(function () {
        newComputer.tagovi.forEach((tag) => {
          const params3 = [
            tag,
            newComputer.tip_proizvoda,
            newComputer.podtip_proizvoda,
            newComputer.proizvodjac,
            newComputer.tip,
            date_time,
          ];
          client.execute(query3, params3);
        });
        res.render("index", { alertType: "success", msg: "Computer has been created" });
      });
    });
  } else {
    res.render("index", { alertType: "danger", msg: "Something went wrong" });
  }
});

routerCreateUpdateComputer.put("/put/:id", (req, res) => {});

routerCreateUpdateComputer.get("/delete/:id", (req, res) => {});

module.exports = routerCreateUpdateComputer;
