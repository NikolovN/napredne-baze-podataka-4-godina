const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const Uuid = require("cassandra-driver").types.Uuid;
const uuid = require("uuid");
const express = require("express");
const routerRegister = express.Router();
const Mapper = cassandra.mapping.Mapper;
var async = require("async");
//https://www.youtube.com/watch?v=voDummz1gO0&ab_channel=CodAffection

routerRegister.get("/", (req, res) => {
  res.render("register", { title: "Create account", user: { _id: "", ime: "", prezime: "", email: "", password: "" } });
});

routerRegister.get("/:id", (req, res) => {
  const query = "SELECT * FROM ecatalog.korisnici WHERE korisnikId = ?";
  const id = req.params.id;
  console.log("params", req.params.id);
  client.execute(query, [id], { prepare: true }).then(function (result) {
    res.render("register", {
      title: "Update user",
      user: {
        _id: result.rows[0].korisnikid.toString(),
        ime: result.rows[0].ime,
        prezime: result.rows[0].prezime,
        email: result.rows[0].email,
      },
    });
  });
});

routerRegister.post("/", (req, res) => {
  var currentTime = new Date();
  var month = currentTime.getMonth() + 1;
  var day = currentTime.getDate();
  var year = currentTime.getFullYear();
  var date_time = new Date(year, month, day);
  if (req.body._id == "") {
    const id = Uuid.random();
    const newUser = {
      id: id,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: req.body.password,
    };

    console.log("New user", newUser);
    client
      .execute(
        "CREATE KEYSPACE IF NOT EXISTS ecatalog WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '3' }"
      )
      .then(function () {
        const query =
          "CREATE TABLE IF NOT EXISTS ecatalog.korisnici (korisnikId uuid, ime text, prezime text, email text, datum_kreiranja timestamp, PRIMARY KEY (korisnikId))";
        return client.execute(query);
      })
      .then(function () {
        const query =
          "CREATE TABLE IF NOT EXISTS ecatalog.korisnici_logovanje (email text, password text, korisnikId uuid, PRIMARY KEY (email))";
        return client.execute(query);
      })
      .then(function () {
        const query =
          "INSERT INTO ECatalog.korisnici (korisnikId, ime, prezime, email, datum_kreiranja) VALUES (?, ?, ?, ?, ?)";
        return client.execute(query, [newUser.id, newUser.firstName, newUser.lastName, newUser.email, date_time], {
          prepare: true,
        });
      })
      .then(function () {
        const query = "INSERT INTO ecatalog.korisnici_logovanje (email, password, korisnikId) VALUES (?, ?, ?)";
        return client.execute(query, [newUser.email, newUser.password, newUser.id], {
          prepare: true,
        });
      })
      .then(function () {
        return client.execute("SELECT * FROM ecatalog.korisnici");
      })
      .then(function (result) {
        result.rows.forEach((row) => {
          console.log(row);
        });
      })
      .catch(function (err) {
        console.error("There was an error", err);
        return client.shutdown().then(() => {
          throw err;
        });
      });
  } else {
    const korisnikid = req.params.id;
    modifiedUser = {
      korisnikid: req.body._id,
      ime: req.body.firstName,
      prezime: req.body.lastName,
      email: req.body.email,
      password: req.body.password,
    };

    console.log("Modified", modifiedUser);

    const query = "UPDATE ecatalog.korisnici SET ime = ?, prezime = ?, email = ? WHERE korisnikid = ?";
    console.log("first tag");
    client
      .execute(query, [modifiedUser.ime, modifiedUser.prezime, modifiedUser.email, modifiedUser.korisnikid])
      .then(() => {
        console.log("second tag");
        const query = "UPDATE ecatalog.korisnici_logovanje SET password = ?, korisnikid = ? WHERE email = ?";
        client.execute(query, [modifiedUser.password, modifiedUser.korisnikid, modifiedUser.email]);
      })
      .catch(function (err) {
        console.error("There was an error", err);
        return client.shutdown().then(() => {
          throw err;
        });
      });
  }

  res.redirect("/");
});

routerRegister.put("/put/:id", (req, res) => {});

routerRegister.get("delete/:id", (req, res) => {});

module.exports = routerRegister;
