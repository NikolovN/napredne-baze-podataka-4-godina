const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const Mapper = cassandra.mapping.Mapper;

const UnderscoreCqlToCamelCaseMappings = cassandra.mapping.UnderscoreCqlToCamelCaseMappings;

const mappingOptions = {
  models: {
    Product: {
      tables: ["products"],
      keyspace: "ecatalog",
      columns: {
        productId: "productId",
        name: "name",
        price: "price",
      },
      mappings: new UnderscoreCqlToCamelCaseMappings(),
    },
  },
};

const mapper = new Mapper(client, mappingOptions);

module.exports = mapper;
