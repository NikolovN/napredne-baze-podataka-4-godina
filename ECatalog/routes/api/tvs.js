const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const uuid = require("uuid");
const express = require("express");
const routerTVs = express.Router();
const mapper = require("./mapper");

routerTVs.get("/", (req, res) => {
  client
    .execute("SELECT * FROM ecatalog.tvs")
    .then(function (result) {
      tvs = [];
      result.rows.forEach((row) => {
        const date = "Jan " + row.datum_dodavanja.toString().substring(8, 15);
        const array = [];
        const url_params = row.proizvodjac + row.tip;
        row.tagovi.forEach((tag) => {
          array.push(tag);
        });
        console.log(row.hdd_ssd);
        tvs.push({
          url_params: url_params,
          proizvodjac: row.proizvodjac,
          tip: row.tip,
          tip_proizvoda: row.tip_proizvoda,
          podtip_proizvoda: row.podtip,
          tip_ekrana: row.tip_ekrana,
          diagonala_ekrana: row.diagonala_ekrana,
          rezolucija: row.rezolucija,
          digitalni_tjuner: row.digitalni_tjuner,
          operativni_sistem: row.operativni_sistem,
          cena: row.cena,
          tagovi: array,
          datum_dodavanja: date,
        });
      });
      res.render("tvs", {
        tvs,
      });
    })
    .catch(function (err) {
      console.error("There was an error", err);
      return client.shutdown().then(() => {
        throw err;
      });
    });
});

routerTVs.get("/:id", (req, res) => {});

routerTVs.post("/", (req, res) => {});

routerTVs.put("/:id", (req, res) => {});

routerTVs.delete("/:id", (req, res) => {});

module.exports = routerTVs;
