const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const Uuid = require("cassandra-driver").types.Uuid;
const uuid = require("uuid");
const express = require("express");
const routerCreateUpdateTvs = express.Router();
const Mapper = cassandra.mapping.Mapper;
var async = require("async");

routerCreateUpdateTvs.get("/", (req, res) => {
  res.render("create-update-tvs", {
    korisnikid: "",
    method: "create",
    title: "Create TV",
    tv: {
      proizvodjac: "SONY",
      tip: "Televizor KDL50WF665 SMART (Crni)",
      tip_proizvoda: "TV",
      podtip_proizvoda: "smart",
      tip_ekrana: "LED",
      diagonala_ekrana: '50" (127 cm)',
      rezolucija: "1080p Full HD",
      digitalni_tjuner: "DVB-T/T2/C/S/S2",
      operativni_sistem: "Linux",
      cena: "59.999RSD",
      tagovi: "sony tv led smart black",
    },
    params: {},
    readonly: "",
  });
});

routerCreateUpdateTvs.get("/:id", (req, res) => {
  console.log(req.params.id);
  res.render("index", { alertType: "info", msg: req.params.id });
});

function getTagsArray(tags) {
  var words = [];
  words = tags.split(" ");
  return words;
}

routerCreateUpdateTvs.post("/", (req, res) => {
  if (req.body.method === "update") {
    var olddatumdodavanja = req.body.olddatumdodavanja;
    const year = olddatumdodavanja.substring(0, 4);
    const month = olddatumdodavanja.substring(5, 7);
    const day = olddatumdodavanja.substring(8, 10);
    var yearr = parseInt(year);
    var monthh = parseInt(month);
    var dayy = parseInt(day);
    console.log(yearr + " " + monthh + " " + dayy);
    olddatumdodavanja = new Date(yearr, monthh, dayy);
    console.log("Datum", olddatumdodavanja.toString());
    var tags = [];
    var oldkorisnikid = req.body.oldkorisnikid;
    tags = getTagsArray(req.body.tagovi);
    const updatedTv = {
      proizvodjac: req.body.proizvodjac,
      tip: req.body.tip,
      korisnikid: oldkorisnikid,
      tip_proizvoda: "TV",
      podtip_proizvoda: req.body.podtip_proizvoda,
      tip_ekrana: req.body.tip_ekrana,
      diagonala_ekrana: req.body.diagonala_ekrana,
      rezolucija: req.body.rezolucija,
      digitalni_tjuner: req.body.digitalni_tjuner,
      operativni_sistem: req.body.operativni_sistem,
      cena: req.body.cena,
      tagovi: tags,
      datum_dodavanja: olddatumdodavanja,
    };


    var array1 = [];
    updatedTv.tagovi.forEach((tag) => {
      if (tag === "") {
      } else {
        array1.push(tag);
      }
    });
    updatedTv.tagovi = array1;
    console.log(array1);
    console.log("Updated TV", updatedTv);
    client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then((result) => {
      const korisnikid = result.rows[0].korisnikid.toString();
      console.log("Korisnik koji dodaje novi TV: " + korisnikid);
      const query1 =
        "UPDATE ecatalog.tvs SET korisnikid = ?,  tip_proizvoda = ?, podtip = ?,  tip_ekrana = ?, diagonala_ekrana = ?, rezolucija = ?, digitalni_tjuner = ?, operativni_sistem = ?, cena = ?, tagovi = ?, datum_dodavanja = ? WHERE proizvodjac = ? AND tip = ?";
      const query2 =
        "UPDATE ecatalog.proizvodi_po_datumu_dodavanja SET proizvodjac = ? WHERE tip_proizvoda = ? AND podtip = ? AND datum_dodavanja = ? AND tip = ?";
      const query3 =
        "INSERT INTO ecatalog.proizvodi_po_tagovima (tag, tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?)";
      const query4 =
        "UPDATE ecatalog.korisnik_dodao_produkt SET tip_proizvoda = ?, podtip = ?, proizvodjac = ? WHERE korisnikid = ? AND tip = ? AND added_date = ?";
      const params1 = [
        updatedTv.korisnikid,
        updatedTv.tip_proizvoda,
        updatedTv.podtip_proizvoda,
        updatedTv.tip_ekrana,
        updatedTv.diagonala_ekrana,
        updatedTv.rezolucija,
        updatedTv.digitalni_tjuner,
        updatedTv.operativni_sistem,
        updatedTv.cena,
        updatedTv.tagovi,
        updatedTv.datum_dodavanja,
        req.body.oldproizvodjac,
        req.body.oldtip,
      ];

      console.log(params1);
      const params2 = [
        updatedTv.proizvodjac,
        req.body.oldtipprodukta,
        req.body.oldpodtipprodukta,
        olddatumdodavanja,
        updatedTv.tip,
      ];

      const params4 = [
        updatedTv.tip_proizvoda,
        updatedTv.podtip_proizvoda,
        updatedTv.proizvodjac,
        oldkorisnikid,
        req.body.oldtip,
        olddatumdodavanja,
      ];

      const queries = [
        { query: query1, params: params1 },
        { query: query2, params: params2 },
        { query: query4, params: params4 },
      ];
      console.log(queries);
      client.batch(queries, { prepare: true }).then(function () {
        console.log("batch finished updating...");
        oldtags = getTagsArray(req.body.oldtagovi);
        oldtags = oldtags.filter((tagg) => {
          return tagg != "";
        });
        console.log(oldtags);
        oldtags.forEach((tag) => {
          console.log(tag);
          client.execute(
            "DELETE FROM ecatalog.proizvodi_po_tagovima WHERE tag = ? AND tip = ? AND datum_dodavanja = ?",
            [tag, req.body.oldtip, olddatumdodavanja]
          );
        });
        updatedTv.tagovi = updatedTv.tagovi.filter((tagg) => {
          return tagg != "";
        });
        updatedTv.tagovi.forEach((tag) => {
          const params3 = [
            tag,
            updatedTv.tip_proizvoda,
            updatedTv.podtip_proizvoda,
            updatedTv.proizvodjac,
            updatedTv.tip,
            olddatumdodavanja,
          ];
          client.execute(query3, params3);
        });
        res.render("index", { alertType: "success", msg: "TV has been updated: " + updatedTv.tip });
      });
    });
  } else if (req.body.method === "create") {
    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var tags = [];
    tags = getTagsArray(req.body.tagovi);
    var date_time = new Date(year, month, day);
    console.log("First phone tag");
    const newTv = {
      proizvodjac: req.body.proizvodjac,
      tip: req.body.tip,
      tip_proizvoda: "TV",
      podtip_proizvoda: req.body.podtip_proizvoda,
      tip_ekrana: req.body.tip_ekrana,
      diagonala_ekrana: req.body.diagonala_ekrana,
      rezolucija: req.body.rezolucija,
      digitalni_tjuner: req.body.digitalni_tjuner,
      operativni_sistem: req.body.operativni_sistem,
      cena: req.body.cena,
      tagovi: tags,
      datum_dodavanja: date_time,
    };
    console.log("New TV", newTv);

    client.execute("SELECT korisnikid FROM ecatalog.trenutno_ulogovani_korisnik WHERE id = 0").then((result) => {
      const korisnikid = result.rows[0].korisnikid.toString();
      console.log("Korisnik koji dodaje novi tv: " + korisnikid);
      const query1 =
        "INSERT INTO ecatalog.tvs (proizvodjac, tip, korisnikid, tip_proizvoda, podtip, tip_ekrana, diagonala_ekrana, rezolucija, digitalni_tjuner, operativni_sistem, cena, tagovi, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
      const query2 =
        "INSERT INTO ecatalog.proizvodi_po_datumu_dodavanja (tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?)";
      const query3 =
        "INSERT INTO ecatalog.proizvodi_po_tagovima (tag, tip_proizvoda, podtip, proizvodjac, tip, datum_dodavanja) VALUES (?, ?, ?, ?, ?, ?)";
      const query4 =
        "INSERT INTO ecatalog.korisnik_dodao_produkt (korisnikid, tip_proizvoda, podtip, proizvodjac, tip, added_date) VALUES (?, ?, ?, ?, ?, ?)";
      const params1 = [
        newTv.proizvodjac,
        newTv.tip,
        korisnikid,
        newTv.tip_proizvoda,
        newTv.podtip_proizvoda,
        newTv.tip_ekrana,
        newTv.diagonala_ekrana,
        newTv.rezolucija,
        newTv.digitalni_tjuner,
        newTv.operativni_sistem,
        newTv.cena,
        newTv.tagovi,
        date_time,
      ];

      const params2 = [newTv.tip_proizvoda, newTv.podtip_proizvoda, newTv.proizvodjac, newTv.tip, date_time];

      const params4 = [
        korisnikid,
        newTv.tip_proizvoda,
        newTv.podtip_proizvoda,
        newTv.proizvodjac,
        newTv.tip,
        date_time,
      ];

      const queries = [
        { query: query1, params: params1 },
        { query: query2, params: params2 },
        { query: query4, params: params4 },
      ];
      client.batch(queries, { prepare: true }).then(function () {
        newTv.tagovi.forEach((tag) => {
          const params3 = [tag, newTv.tip_proizvoda, newTv.podtip_proizvoda, newTv.proizvodjac, newTv.tip, date_time];
          client.execute(query3, params3);
        });
        res.render("index", { alertType: "success", msg: "TV has been created" });
      });
    });
  } else {
    res.render("index", { alertType: "danger", msg: "Something went wrong" });
  }
});

routerCreateUpdateTvs.put("/put/:id", (req, res) => {});

routerCreateUpdateTvs.get("/delete/:id", (req, res) => {});

module.exports = routerCreateUpdateTvs;
