const cassandra = require("cassandra-driver");
const client = require("./cassandra-client");
const uuid = require("uuid");
const express = require("express");
const routerPhones = express.Router();
const mapper = require("./mapper");

routerPhones.get("/", (req, res) => {
  client
    .execute("SELECT * FROM ecatalog.phones")
    .then(function (result) {
      phones = [];
      result.rows.forEach((row) => {
        const date = "Jan " + row.datum_dodavanja.toString().substring(8, 15);
        const array = [];
        const url_params = row.proizvodjac + row.tip;
        row.tagovi.forEach((tag) => {
          array.push(tag);
        });

        phones.push({
          url_params: url_params,
          proizvodjac: row.proizvodjac,
          tip: row.tip,
          tip_proizvoda: row.tip_proizvoda,
          podtip_proizvoda: row.podtip,
          diagonala_ekrana: row.diagonala_ekrana,
          ram_memorija: row.ram_memorija,
          interna_memorija: row.interna_memorija,
          kamera: row.kamera,
          kapacitet_baterije: row.kapacitet_baterije,
          rezolucija: row.rezolucija,
          tip_ekrana: row.tip_ekrana,
          cena: row.cena,
          tagovi: array,
          datum_dodavanja: date,
        });
      });
      res.render("phones", {
        phones,
      });
    })
    .catch(function (err) {
      console.error("There was an error", err);
      return client.shutdown().then(() => {
        throw err;
      });
    });
});

routerPhones.get("/:id", (req, res) => {});

routerPhones.post("/", (req, res) => {});

routerPhones.put("/:id", (req, res) => {});

routerPhones.delete("/:id", (req, res) => {});

module.exports = routerPhones;
