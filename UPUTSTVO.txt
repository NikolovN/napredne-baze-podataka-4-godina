1) Potrebno je instalirati nodejs, ukoliko vec nije instaliran (14.15.4 LTS)
	https://nodejs.org/en/
	

2) Podici cassandrin server u pozadini

3) Putem noSql managera ili noSql viewer-a izvrsiti upite koji se nalaze u tekstualnom dokumentu (cql-schemas.txt)
	Time kreirati keyspace i potrebne tabele.

4) Otvoriti command prompt i preci u folder programa. To je folder pod nazivom ECatalog i sadrzi package.json i index.js
	cd ....\putanja\ECatalog

5) U command promptu izvrsiti naredbu (potreban internet - skinuce sve pakete koji su potrebni da aplikacija radi)
	npm install

6) Poslednja komanda (pokrenuce express server i web aplikaciji se moze pristupiti putem localhost:5000)
	npm run dev

7) Prva stranica koja se otvara je login... Preci na register i napraviti nalog... Ulogovati se

8) Za samu aplikaciju je potreban internet jer su korisceni linkovi ka bootstrapu, datatables i slicno.

9) Napomene:
	-Profilna strana sadrzi podatke o korisniku i podatke o svim produktima koje je taj korisnik dodao
	-Dugme fill database ce popuniti cassandrinu bazu sa po 50 korisnika i sa po 50 proizvoda bilo kog tipa (FAKE podaci)
	-Forme za dodavanje novih proizvoda vec imaju defaultne vrednosti radi brzeg testiranja, mogu se naravno menjati
	-Show all products prikazuje sve produkte koji se nalaze u bazi bilo kog tipa
	-Svaki produkt u svojoj tabeli sadrzi listu tagova gde svaki predstavlja link ka tabeli koja prikazuje sve 
		produkte bilo kog tipa koji sadrze isti tag

	