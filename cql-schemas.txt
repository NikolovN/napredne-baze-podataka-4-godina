CREATE KEYSPACE ecatalog WITH replication = {'class':'SimpleStrategy', 'replication_factor': 3};

CREATE TABLE IF NOT EXISTS ecatalog.korisnici_logovanje (
    email text,
    password text,
    korisnikid uuid,
    PRIMARY KEY (email)
);

CREATE TABLE IF NOT EXISTS ecatalog.korisnici (
    korisnikId uuid,
    ime text,
    prezime text,
    email text,
    datum_kreiranja timestamp,
    PRIMARY KEY (korisnikId)
);

CREATE TABLE IF NOT EXISTS ecatalog.trenutno_ulogovani_korisnik (
    id int,
    korisnikid uuid,
    PRIMARY KEY (id)
);

    CREATE TABLE IF NOT EXISTS ecatalog.phones (
    proizvodjac text,
    tip text,
    korisnikid uuid,
    tip_proizvoda text, 
    podtip text,
    diagonala_ekrana text,
    ram_memorija int,
    interna_memorija int,
    kamera text,
    kapacitet_baterije text,
    rezolucija text,
    tip_ekrana text,
    cena text,
    tagovi set<text>,
    datum_dodavanja timestamp,
    PRIMARY KEY (proizvodjac, tip)
);



    CREATE TABLE IF NOT EXISTS ecatalog.computers (
    proizvodjac text,
    tip text,
    korisnikid uuid,
    tip_proizvoda text, 
    podtip text,
    model_procesora text,
    diagonala_ekrana text,
    tip_graficke_kartice text,
    ram_memorija int,
    HDD_SSD text,
    cena text,
    tagovi set<text>,
    datum_dodavanja timestamp,
    PRIMARY KEY (proizvodjac, tip)
);

    CREATE TABLE IF NOT EXISTS ecatalog.tvs (
    proizvodjac text,
    tip text,
    korisnikid uuid,
    tip_proizvoda text, 
    podtip text,
    tip_ekrana text,
    diagonala_ekrana text,
    rezolucija text,
    digitalni_tjuner text,
    operativni_sistem text,
    cena text,
    tagovi set<text>,
    datum_dodavanja timestamp,
    PRIMARY KEY (proizvodjac, tip)
);

    CREATE TABLE IF NOT EXISTS ecatalog.proizvodi_po_datumu_dodavanja (
    tip_proizvoda text,
    podtip text,
    proizvodjac text,
    tip text,
    datum_dodavanja timestamp,
    PRIMARY KEY ((tip_proizvoda, podtip), datum_dodavanja, tip)
    )WITH CLUSTERING ORDER BY (datum_dodavanja DESC, tip ASC);

CREATE TABLE IF NOT EXISTS ecatalog.proizvodi_po_tagovima (
    tag text,
    tip_proizvoda text,
    podtip text,
    proizvodjac text,
    tip text,
    datum_dodavanja timestamp,
    PRIMARY KEY (tag, tip, datum_dodavanja)
)WITH CLUSTERING ORDER BY (tip ASC, datum_dodavanja DESC);

CREATE TABLE IF NOT EXISTS ecatalog.korisnik_dodao_produkt (
    korisnikid uuid,
    tip_proizvoda text,
    podtip text,
    proizvodjac text,
    tip text,
    added_date timestamp,
    PRIMARY KEY (korisnikid, tip, added_date)
)




 client
    .execute(
      "CREATE KEYSPACE IF NOT EXISTS ecatalog WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '3' }"
    )
    .then(() => {
      client.execute(
        "CREATE TABLE IF NOT EXISTS ecatalog.phones (proizvodjac text, tip text, korisnikid uuid, tip_proizvoda text, podtip text, diagonala_ekrana text, ram_memorija int, interna_memorija int, kamera text, kapacitet_baterije text, rezolucija text, tip_ekrana text, cena text, tagovi set<text>, datum_dodavanja timestamp, PRIMARY KEY (proizvodjac, tip))"
      );
      client.execute(
        "CREATE TABLE IF NOT EXISTS ecatalog.proizvodi_po_datumu_dodavanja (tip_proizvoda text, podtip text, proizvodjac text, tip text, datum_dodavanja timestamp, PRIMARY KEY ((tip_proizvoda, podtip), datum_dodavanja, tip)) WITH CLUSTERING ORDER BY (datum_dodavanja DESC, tip ASC)"
      );
      client.execute(
        "CREATE TABLE IF NOT EXISTS ecatalog.proizvodi_po_tagovima (tag text, tip_proizvoda text, podtip text, proizvodjac text, tip text, datum_dodavanja timestamp, PRIMARY KEY (tag, tip, datum_dodavanja)) WITH CLUSTERING ORDER BY (tip ASC, datum_dodavanja DESC)"
      );
      client.execute(
        "CREATE TABLE IF NOT EXISTS ecatalog.korisnik_dodao_produkt (korisnikid uuid, tip_proizvoda text, podtip text, proizvodjac text, tip text, added_date timestamp, PRIMARY KEY (korisnikid, tip, added_date))  WITH CLUSTERING ORDER BY (tip ASC, added_date DESC)"
      );
      client.execute(
        "CREATE TABLE IF NOT EXISTS ecatalog.computers (proizvodjac text, tip text, korisnikid uuid, tip_proizvoda text, podtip text,  model_procesora text, diagonala_ekrana text, tip_graficke_kartice text, ram_memorija int, HDD_SSD text, cena text, tagovi set<text>, datum_dodavanja timestamp, PRIMARY KEY (proizvodjac, tip))"
      );
      client.execute(
        "CREATE TABLE IF NOT EXISTS ecatalog.tvs (proizvodjac text, tip text, korisnikid uuid, tip_proizvoda text, podtip text,  tip_ekrana text, diagonala_ekrana text, rezolucija text, digitalni_tjuner text, operativni_sistem text, cena text, tagovi set<text>, datum_dodavanja timestamp, PRIMARY KEY (proizvodjac, tip))"
      );
      client.execute(
        "CREATE TABLE IF NOT EXISTS ecatalog.korisnici_logovanje (email text, password text, korisnikid uuid, PRIMARY KEY (email));"
      );
      client.execute(
        "CREATE TABLE IF NOT EXISTS ecatalog.korisnici (korisnikId uuid, ime text, prezime text, email text, datum_kreiranja timestamp, PRIMARY KEY (korisnikId));"
      );
      client
        .execute(
          "CREATE TABLE IF NOT EXISTS ecatalog.trenutno_ulogovani_korisnik (id int, korisnikid uuid, PRIMARY KEY (id)) "
        )
        .then(() => {
          client
            .execute("INSERT INTO ecatalog.trenutno_ulogovani_korisnik (id, korisnikid) VALUES (0, null)")
            .then(() => {
              res.render("login");
            });
        });
    });